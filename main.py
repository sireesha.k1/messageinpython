import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from scripts.core.find_avg_min_max import max_col, min_col, avg_col, min_timestamp, max_timestamp


def sender(recipients):
    body = f'Maximum values:{str(max_col)}\nMinimum values:{str(min_col)}\nAverages:{str(avg_col)}\nMinimum Timestamps:{str(min_timestamp)}\nMaximum Timestamps:{str(max_timestamp)}'
    msg = MIMEMultipart()

    msg['Subject'] = 'Message in Python'
    msg['From'] = 'sireesha.k@knowledgelens.com'
    msg['To'] = (', ').join(recipients.split(','))

    msg.attach(MIMEText(body, 'plain'))
    with open("E:\\xldata.csv", 'rb') as file:
        msg.attach(MIMEApplication(file.read(), Name="xldata.csv"))
    msg.attach(MIMEText(body, 'plain'))
    with open("E:\\taskdata.csv", 'rb') as file:
        msg.attach(MIMEApplication(file.read(), Name="taskdata.csv"))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login('sireesha.k@knowledgelens.com', 'Sireeshaks@11')
    server.send_message(msg)
    server.quit()


if __name__ == '__main__':
    sender('sireesha.k@knowledgelens.com')
    #sender('mamatha.bk@knowledgelens.com,nandini.kopparthi@knowledgelens.com,pooja.mulaguri@knowledgelens.com')
