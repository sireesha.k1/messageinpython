import pandas as pd
import csv

df = pd.read_csv('E:\\xldata.csv')
max_col = df.max()
min_col = df.min()
avg_col = df.mean()
with open('E:\\xldata.csv', 'r') as file:
    reader = csv.reader(file)
    k1 = list(reader)
    column_count = len(k1[0])
    l = dict()
    max_col = list(max_col)
    min_col = list(min_col)
    avg_col = list(avg_col)
    avg_coln=['-']
    for i in avg_col:
        avg_coln.append(i)
    avg_col=avg_coln
    max_timestamp, min_timestamp = ["-"], ["-"]
    for i in range(1, column_count):
        for j in range(1, len(k1)):
            if (float(max_col[i]) == float(k1[j][i])):
                max_timestamp.append(k1[j][0])
            if (float(min_col[i]) == float(k1[j][i])):
                min_timestamp.append(k1[j][0])
    #print(len(avg_col),avg_col,min_timestamp,max_timestamp)
    min_timestamp.insert(3, "-")
    #print(column_count)

    with open('E:\\taskdata.csv', mode='w') as csv_file:
        fieldnames = ["Name", "min_val", "max_val","average","min_timestamp","max_timestamp"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for i in range(0, column_count):
            writer.writerow({"Name": k1[0][i], "min_val": min_col[i], "max_val": max_col[i],"average":avg_col[i],"min_timestamp":min_timestamp[i],"max_timestamp":max_timestamp[i]})
